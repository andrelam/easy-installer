# Easy Installer
**Current Version:** v0.11.0-bêta

Members:
- Gaël
- Romain
- Alexis
- Vincent
- Arnau
- Manoj
- Omer

developer: [Vincent Bourgmayer](vincent.bourgmayer@e.email)


## Changelog for next Version (only finished element are listed)
### v0.11.1-bêta
- Add word-wrapping in feedback field (issue #242)
- Fix Samsung Galaxy S9 and Samsung Galaxy S9+ instruction at the last step (issue #236)
- Update Fairphone 3's instructions about unlocking bootloader: Warn the user about the 5 seconds timeout (issue #233)
- Add mention about USB 3 port in first view (issue #244)
- Do not display "Congratulation" view if user continue after failure (issue #216)

## Documentation
- [How to build from source with Gradle](../../wikis/Build-with-Gradle)
- [How to support new Device](../../wikis/Support-new-device)
- [How translation works](../../wikis/update-translation)



- [Weblate](https://i18n.e.foundation/projects/e/easy-installer/)
## Techno dependancy
- Java
- FXML < XML
- Groovy ? 
- YAML
- CSS

## Dependancy:
- Java 11+
- JavaFX 13+
- Flash-lib
- Gradle 4.10+
    - org.openjfx.javafxplugin
    - https://badass-jlink-plugin.beryx.org/
- SnakeYaml 1.24+ 
- Git
- Gitlab

