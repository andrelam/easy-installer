#!/bin/bash

# Copyright (C) 2020 ECORP SAS - Author: Vincent Bourgmayer
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Parameter
# $1: The folder where adb runnable is stored
# $2: serial number of device

# Exit status
# - 0 : OEM unlocked on device
# - 10 : Missing ADB folder path
# - 11 : Missing device ID
# - 12: can't get IMEI

ADB_FOLDER_PATH=$1

DEVICE_ID=$2
if [ -z "$ADB_FOLDER_PATH" ]
then
  exit 10
fi

if [ -z "$DEVICE_ID" ]
then
  exit 11
fi


ADB_PATH=${ADB_FOLDER_PATH}"adb"
echo "ADB path: $ADB_PATH"

# get Imei
DEVICE_IMEI=$($ADB_PATH shell "service call iphonesubinfo 1 | cut -c 52-66 | tr -d '.[:space:]'")
if [ -z "$DEVICE_IMEI" ]
then
  exit 12
fi

URL="https://factory.fairphone.com/api/unlock-codes/$DEVICE_IMEI/$DEVICE_ID"
RESULT=$(curl -X POST $URL)

echo "${RESULT:9:8}"