/*
 * Copyright 2019-2020 - ECORP SAS 

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ecorp.easy.installer.controllers;

import ecorp.easy.installer.AppConstants;
import ecorp.easy.installer.controllers.subcontrollers.AbstractSubController;
import ecorp.easy.installer.EasyInstaller;
import ecorp.easy.installer.models.Device;
import ecorp.easy.installer.threads.ThreadFactory;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.Initializable;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.event.EventHandler;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.control.Button;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 * Main window Controller class
 *
 * @author Vincent Bourgmayer
 * @author Ingo
 */
public class MainWindowController implements Initializable {
    private final static Logger logger = LoggerFactory.getLogger(MainWindowController.class);
    @FXML AnchorPane root;
    @FXML Button nextButton;
    @FXML Label titleLabel;
    
    ResourceBundle i18n ;//internationalization
    String currentSubRootId =null;
    ThreadFactory factory = new ThreadFactory("/yaml/");
    Device device; //Object encapsulating data about the device to flash
    private boolean isFlashed = false; // True when /e/ OS installed on device


    /**
     * Initializes the controller class.
     * @param url
     * @param rb The resourceBundle used for internationalization
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        logger.info("initialize()");
        this.i18n = rb;
        disableNextButton(false); //just for test
        loadSubScene();
        DoubleProperty fontSize = new SimpleDoubleProperty(0);
        
        fontSize.bind(root.widthProperty().add(root.heightProperty()).divide(123.2)); //72 = 1440 (default width): 20 (default font size). 51.2 = 1024 / 20        
        root.styleProperty().bind(
            Bindings.concat("-fx-font-size: ", fontSize.asString("%.0f")).concat("px;")
        );
        
    }
    
    /**
     * Open URL of help 
     */
    public void onNeedHelpBtnClick(){ openUrlInBrowser("http://e.foundation"); }
    
    /**
     * open URL of donation
     */
    public void onDonateBtnClick(){  openUrlInBrowser("https://e.foundation/donate/"); }
    
    /**
     * Event happening when using click on "NextButton"
     */
    public void onNextBtnClick(){ loadSubScene(); }
    
    /**
     * Define if nextButton is disable or not
     * @param value if true the button will be disable. If false it won't be.
     */
    synchronized public void disableNextButton(boolean value){
        nextButton.setDisable(value);
    }
        
    /**
     * Set the new text Of NextButton
     * @param newTextKey specify the key that should be use by ResourceBundle
     */
    public void setNextButtonText(String newTextKey){
        nextButton.setText(i18n.getString(newTextKey));
    }
    /**
     * Change the handler for when next button is clicked
     * @param eh the new event Handler
     */
    public void setNextButtonOnClickListener(EventHandler<MouseEvent> eh){
        nextButton.setOnMouseClicked(eh);
    }
    
    /**
     * Hide the Next button
     * @param value true to hide, false either
     */
    synchronized public void setNextButtonVisible(boolean value){
        nextButton.setVisible(value);
    }
    
    /**
     * Reset the handler for when next button is clicked
     */
    public void resetNextButtonEventHandler(){
        logger.info("ResetNextButtonEventHandler()");
        nextButton.setOnMouseClicked((MouseEvent event)->{
            onNextBtnClick();
        });
    }
    
    /**
     * Change title in the currentView
     * @param titleKey key for translation
     */
    public void setViewTitle(String titleKey){
        this.titleLabel.setText(i18n.getString(titleKey));
    }
    
    /**
     * Tell MainController that device is flashed or not
     * @param isFlashed 
     */
    public void setIsFlashed(boolean isFlashed) {
        this.isFlashed = isFlashed;
    }
    
    /**
     * Load different group of controls depending of the current step (of whole process)
     */
    public void loadSubScene(){
        logger.info("loadSubScene("+currentSubRootId+")");
        
        if(currentSubRootId == null || currentSubRootId.isEmpty()){
            currentSubRootId =loadSubUI("1-beforeYouBegin.fxml");
            titleLabel.setText(i18n.getString("before_mTitle"));
        }else{
            switch(currentSubRootId){
                case "beforeYouBeginRoot":
                    removeNodeFromRoot(currentSubRootId);
                    currentSubRootId = loadSubUI("2-connectDevice.fxml");
                    this.titleLabel.setText(i18n.getString("connect_mTitle"));
                    break;
                case "connectDeviceRoot":
                    removeNodeFromRoot(currentSubRootId);
                    currentSubRootId = loadSubUI("3-enableADB.fxml");
                    this.titleLabel.setText(i18n.getString("devMode_mTitle"));
                    break;
                case "enableDevMode":
                    removeNodeFromRoot(currentSubRootId);
                    currentSubRootId = loadSubUI("4-deviceDetected.fxml");
                    this.titleLabel.setText(i18n.getString("detect_mTitle"));
                    disableNextButton(true);
                    break;
                case "deviceDetectedRoot":
                    removeNodeFromRoot(currentSubRootId);
                    currentSubRootId = loadSubUI("5-downloadSrc.fxml");
                    this.titleLabel.setText(i18n.getString("download_mTitle"));
                    disableNextButton(true);
                    break;
                case "downloadSceneRoot":
                    removeNodeFromRoot(currentSubRootId);
                    currentSubRootId = loadSubUI("6-flashScene.fxml");
                    this.titleLabel.setText(i18n.getString("installationTitle"));
                    break;
                case "flashSceneRoot":
                    removeNodeFromRoot(currentSubRootId);
                    currentSubRootId= loadSubUI("7-flashResult.fxml");
                    break;
                case "flashResultRoot":
                    removeNodeFromRoot(currentSubRootId);
                    if( isFlashed ){
                        currentSubRootId=loadSubUI("8-congrats.fxml");
                        this.titleLabel.setText(i18n.getString("congrats_mTitle"));
                    }
                    else{
                        currentSubRootId=loadSubUI("9-feedback.fxml");
                        this.titleLabel.setText(i18n.getString("feedback_mTitle"));      
                    }
                    break;
                case "congratsRoot":
                    removeNodeFromRoot(currentSubRootId);
                    currentSubRootId=loadSubUI("9-feedback.fxml");
                    this.titleLabel.setText(i18n.getString("feedback_mTitle"));
                    break;
                case "feedbackRoot":
                    Platform.exit();
                    break;  
                default:
                    logger.error("Invalid currentSubRootId");
                    break;
            }
        }
    }
    
    public void retryToFlash(){
        removeNodeFromRoot(currentSubRootId);
        currentSubRootId = loadSubUI("6-flashScene.fxml");
        this.titleLabel.setText(i18n.getString("installationTitle"));
    }
    
    /**
     * Put device object inside the factory
     * @param device the device object
     */
    public void setDevice(Device device){
        this.factory.changeMould(device);
        this.device = device;
    }
    
    
    /**
     * Remove a node from the root
     * @param nodeId fx:id of the node
     * @return true if element removed
     */
    public boolean removeNodeFromRoot(String nodeId){
        return root.getChildren().removeIf( n -> n.getId().equals(nodeId));
    }
 
    /**
     * Load UI from FXML file
     * @param fxmlName
     * @return 
     */
    public FXMLLoader loadFXML(String fxmlName){
        FXMLLoader loader;
        try{
            loader = new FXMLLoader(getClass().getResource(EasyInstaller.FXML_PATH+fxmlName));
            loader.setResources(i18n);
            loader.load();
            AbstractSubController ctrl = loader.getController();
            if(ctrl != null) ctrl.setParentController(this);
        }catch(IOException e){
            logger.error("fxmlName = {}, error = {}", fxmlName, e.toString());
            return null;
        }
        return loader;
    }
    /**
     * Load the subScene from FXML and return the controller
     * @param fxmlName fxml file name
     * @return subRoot.
     */
    public String loadSubUI(String fxmlName){
        FXMLLoader loader = loadFXML(fxmlName);
        Pane subRoot = (Pane) loader.getRoot();
        root.getChildren().add(0, subRoot); //adding this element as first subNode, let other element like button to still be clickable on small screen
        return subRoot.getId();
    }
    
    /**
     * Show the current UI identified by currentSubRootId
     * It used to get back to FlashScene after eAccount.fxml display
     */
    public void showCurrentSubRoot(){
        this.root.getChildren().forEach(
            ( node)-> { 
                if(node.getId().equals(currentSubRootId)) 
                {
                    node.setManaged(true);
                    node.setVisible(true);
                } 
            });
    }
    
    
// Private method relative to Flash & preparation Thread
    public ThreadFactory getThreadFactory(){
        return factory;
    }
// Other methods
    /**
     * Method call when app is closing
     * It is in charge to close the opened flash or preparation Thread
     */
    public void onStop(){
        logger.debug("onStop()");
    }
    
    /**
     * Open the specified URL in browser depending on the OS
     * Inspired by 
     * https://stackoverflow.com/questions/5226212/how-to-open-the-default-webbrowser-using-java
     * Another solution can be found here:
     * https://stackoverflow.com/questions/16604341/how-can-i-open-the-default-system-browser-from-a-java-fx-application
     * @param url url to open
     */
    public void openUrlInBrowser(String url){
        final String osName = AppConstants.OsName.toLowerCase();
        Runtime rt = Runtime.getRuntime();
        try{
            if(osName.contains("win")){
                rt.exec("rundll32 url.dll,FileProtocolHandler " + url);
            }else if(osName.contains("mac")){
                rt.exec("open " + url);
            }else if(osName.contains("nix") || osName.contains("nux")){
                rt.exec("xdg-open "+url); //https://doc.ubuntu-fr.org/xdg-open
            }
            else{
                throw new Exception("OS not supported"); //Not Supported OS
            }
        }catch(Exception e){
            logger.error("url = {}, error = {}", url, e.toString());
        }
    }
    
    /**
     * Allow subController to access to the device's information
     * @return 
     */
    public Device getDevice() {
        return device;
    }
}
