/*
 * Copyright 2019-2020 - ECORP SAS 

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ecorp.easy.installer.controllers.subcontrollers;


import ecorp.easy.installer.controllers.MainWindowController;
import ecorp.easy.installer.tasks.DownloadTask;
import java.net.URL;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Map;
import java.util.ResourceBundle;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.concurrent.Worker.State;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * FXML Controller class
 *
 * @author Vincent Bourgmayer
 */
public class DownloadSrcController extends AbstractSubController {
    private final static Logger logger = LoggerFactory.getLogger(DownloadSrcController.class);
    private @FXML ProgressBar preparationProgressBar;
    private @FXML Label progressLabel;
    private @FXML Label progressTitle;
    private @FXML Button restartDownloadBtn;
    
    private Map<String, String> sourcesToDownload;
    private Iterator<Map.Entry<String, String>> taskIterator;
    private DownloadService currentService;
    
    @Override    
    public void initialize(URL location, ResourceBundle resources) {
        super.initialize(location, resources);
        restartDownloadBtn.setManaged(false);
        restartDownloadBtn.setVisible(false);
    }

    @Override
    public void setParentController(MainWindowController parentController){
        super.setParentController(parentController);
        sourcesToDownload = parentController.getThreadFactory().getSourcesToDownload();
        taskIterator =  sourcesToDownload.entrySet().iterator();
        startNextDownload();
    }
    
    private boolean startNextDownload(){
        logger.info("startNextDownload()");
        logger.debug("taskIterator has next ? {} "+taskIterator.hasNext());
        if(taskIterator.hasNext()){
            Map.Entry<String, String> source = taskIterator.next();
            
            currentService = new DownloadService(source.getKey(), source.getValue());
            bindProgressUIToService(currentService);
            currentService.start();
            return true;
        }
        return false;
    }
    
    /**
     * Behaviour for when the tryAgainbtn is clicked
     * Called from FXML
     */
    public void onTryAgainBtnClick(){
        if( Arrays.asList( State.RUNNING, State.SCHEDULED, State.READY).contains( currentService.getState() ) )
            currentService.cancel();
        
        currentService.reset();
        bindProgressUIToService(currentService);
        currentService.restart();
        
        this.preparationProgressBar.getStyleClass().remove("errorBar");
        restartDownloadBtn.setManaged(false);
        restartDownloadBtn.setVisible(false); 
    }
    
    
    /** Method called by DownloadServices only */
    
    /**
     * Bind UI properties to Service, and start the download Service
     * @param DownloadService
     */
    private void bindProgressUIToService(DownloadService service){
        logger.info("bindProgressUIToService()");
        //Do some UI binding with download task
        this.progressTitle.textProperty().bind(service.titleProperty());
        this.preparationProgressBar.progressProperty().bind(service.progressProperty());
        this.progressLabel.textProperty().bind(service.messageProperty());
        this.preparationProgressBar.getStyleClass().remove("errorBar");
    }
    
    
    /**
     * Called when each sources had been downloaded and checked successfully
     */
    public void onDownloadsComplete() {
        logger.debug("onDownloadsComplete()");
        preparationProgressBar.setProgress(1.0); //BUG! apparement pas accessible par le service. J'ai un "A bound value cannot be set"
        progressLabel.setText(i18n.getString("download_lbl_complete"));
        progressTitle.setVisible(false);
        parentController.disableNextButton(false);
    }
    
    
    /**
     * Unbind some property of Ui element that indicate progression
     * It is called when Download service failed or succeeded
     */
    private void unbindProgressUI(){
        progressLabel.textProperty().unbind();
        progressTitle.textProperty().unbind();
        preparationProgressBar.progressProperty().unbind();
    }
    
    /**
     * Transform progress bar in error bar
     * @param showErrorProgressLabel true if should display error message
     */
    private void displayErrorProgressBar(boolean showErrorProgressLabel){
        preparationProgressBar.getStyleClass().add("errorBar");
        restartDownloadBtn.setManaged(true);
        restartDownloadBtn.setVisible(true);
        if(showErrorProgressLabel){
            progressLabel.setText(i18n.getString("download_lbl_downloadError"));
        }
    }
    
    /**
     * This is the service (=worker) that run the task
     */
    private class DownloadService extends Service{

        private final String url;
        private final String fileName;
        
        public DownloadService(String url, String fileName){
            this.url = url;
            this.fileName = fileName;
        }
        
        @Override
        protected Task createTask() {
            logger.info("DownloadService.createTask({},{})", url, fileName);
            return new DownloadTask(url, fileName, i18n);
        }
        
        @Override
        protected void succeeded() {
            logger.debug("DownloadService.succeeded()");
            unbindProgressUI();
            if((Boolean) this.getValue() ){ //This read the "value" (boolean) defined at end of run in DownloadTask
                //So here: it returns true if file is downloaded and checksum verified
                if(! startNextDownload() ) onDownloadsComplete(); //if no more download to do, then preparation is over
            }else{ //either download or checksum checks failed
                displayErrorProgressBar(false);
            }
            super.succeeded(); //no sure of its use.
        }

        @Override
        protected void failed() {
            logger.error("DownloadService.failed(), error: {}", this.getException().toString());
            unbindProgressUI();
            displayErrorProgressBar(true);
            super.failed(); //no sure of its use.
        }
    }
}
