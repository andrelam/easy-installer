/*
 * Copyright 2019-2020 - ECORP SAS 

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ecorp.easy.installer.controllers.subcontrollers;

import ecorp.easy.installer.AppConstants;
import ecorp.easy.installer.controllers.MainWindowController;
import ecorp.easy.installer.models.Device;
import ecorp.easy.installer.tasks.UploadToEcloudTask;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.UUID;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.input.MouseEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * FXML Controller class
 *
 * @author Vincent Bourgmayer
 */
public class FeedbackController extends AbstractSubController {
    private final static Logger logger = LoggerFactory.getLogger(FeedbackController.class);
    private @FXML Button sendAnswerBtn;
    private @FXML TextArea commentArea;
    private @FXML Label thanksLabel;
    
    private Button selectedFeelings;
    
    private final static String SELECTED_STYLE="selected-feelings";
    String deviceModel = "undefined";
    
    @Override
    public void setParentController(MainWindowController parentController) {
        super.setParentController(parentController); //To change body of generated methods, choose Tools | Templates.
        parentController.setNextButtonText("feedback_btn_leave");
        Device device = parentController.getDevice();
        if(device != null){
            logger.debug("setParentController(), ParentController.getDevice() != null");
            deviceModel = device.getModel()+", "+device.getDevice();
        }else{
            logger.warn("setParentController(), parentController.getDevice() == null");
        }
        commentArea.setWrapText(true);
    }
    
    
    /**
     * What happened when user click on a feelings button
     * @param event 
     */
    public void onFeelingsSelected(MouseEvent event){
        //When clicking a the already selected feelings, it deselect this event
        Button clickedButton = (Button) event.getSource();
        if(clickedButton.equals(selectedFeelings))  {
            
            logger.debug("onFeelingsSelected(), Deselect: {}", selectedFeelings.getId());
            selectedFeelings.getStyleClass().remove(SELECTED_STYLE);
            selectedFeelings = null;
        }
        else{
            //If another button was selected before, unselect it
            if(selectedFeelings != null) selectedFeelings.getStyleClass().remove(SELECTED_STYLE);
            
            clickedButton.getStyleClass().add(SELECTED_STYLE);
            selectedFeelings =  clickedButton;
            logger.debug("onFeelingsSelected(), select: {}", selectedFeelings.getId());
        }
    }
    
    /**
     * Send the feedback to ecloud
     * @param event 
     */
    public void onSendBtnClicked(MouseEvent event){
        String filePath = createAnswerFile();
        sendAnswerBtn.setDisable(true);
        UploadToEcloudTask uploadTask = new UploadToEcloudTask(AppConstants.FEEDBACK_STORAGE_URL, filePath);
        
        uploadTask.setOnSucceeded(eh -> {
            if( (Boolean) eh.getSource().getValue() ){ //if success
                sendAnswerBtn.setManaged(false);
                sendAnswerBtn.setVisible(false);
                thanksLabel.setVisible(true);
                thanksLabel.setManaged(true);
                logger.debug("onSendBtnClicked(), sending feedback: success");
            }
            else{ //if failure
                sendAnswerBtn.setText(i18n.getString("feedback_btn_sendTryAgain"));
                sendAnswerBtn.setDisable(false);
                logger.error("onSendBtnClicked(), sending feedback: failure");
            }
        });
                
        uploadTask.setOnFailed(eh->{
            sendAnswerBtn.setDisable(false);
            sendAnswerBtn.setText(i18n.getString("feedback_btn_sendTryAgain"));
            logger.error("onSendBtnClicked(), sending feedback: failure, error: {}", eh.getSource().getException().toString());
        });
        Thread uploadTaskThread = new Thread(uploadTask);
        uploadTaskThread.setDaemon(true);
        uploadTaskThread.start();
        
    }
    
    /**
     * Create the file with feedback inside
     * @return 
     */
    private String createAnswerFile(){
        String answerFilePath = AppConstants.getWritableFolder()+"feedback-"+UUID.randomUUID()+".txt";

        try(PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(answerFilePath, true)))) {
            out.write("Timestamp: "+System.currentTimeMillis());
            out.write("\nDevice model: "+this.deviceModel);
            out.write("\nOS name: "+AppConstants.OsName);
            if(selectedFeelings != null){
                out.write("\nFeelings: "+selectedFeelings.getId());
            }
            out.write("\nComment: \n"+this.commentArea.getText());
            
        } catch (IOException e) {
            logger.error("createAnswerFile()\n   answerFilePath = {}\n   error: {}", answerFilePath, e.toString());
         return null;
        }
        return answerFilePath;
    }
}
