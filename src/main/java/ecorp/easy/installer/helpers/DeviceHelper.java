/*
 * Copyright 2019-2020 - ECORP SAS

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ecorp.easy.installer.helpers;
import java.util.HashMap;
/**
 *
 * @author André Lam
 * @author Vincent Bourgmayer
 * @author Ingo
 */
public class DeviceHelper {
    private static final HashMap<String, String> map  = new HashMap<String, String>() {{
        put("hero2lte", "0001");
        put("herolte",  "0002");
        put("star2lte", "0003");
        put("starlte",  "0004");
        put("zeroflte", "0005");
        put("dream2lte", "0006");
        put("dreamlte", "0007");
        put("FP3",      "0008");
    }};


    /**
     * Return internal code for a given device
     * @param key the device code's name (example: Samsung galaxy S7 => herolte)
     * @return can return null if no key doesn't match
     */
    public static String getDeviceInternalcode(String key){
        return map.get(key);
    }
}
