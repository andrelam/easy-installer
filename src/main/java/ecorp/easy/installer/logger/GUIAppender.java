/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ecorp.easy.installer.logger;

import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.AppenderBase;
import java.util.ArrayList;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;

/**
 *
 * @author vincent
 */
public class GUIAppender extends AppenderBase<ILoggingEvent>{
    private ArrayList<Text> logsList= new ArrayList<>();
    private TextFlow container;

    @Override
    protected void append(ILoggingEvent e) {
        if(logsList.size() > 100){
            logsList.remove(0);
        }
        Text txt = buildLabel(e);
        logsList.add(txt);
        
        //side effect
        displayLog(txt);
    }
    
    public synchronized void setContainer(TextFlow container){
        this.container = container;
    }
    
    
    private synchronized void displayLog(Text text){
        if(container != null){
            container.getChildren().add(text);
        }
    }
    private Text buildLabel(ILoggingEvent e){
        
        Text result = new Text(e.getFormattedMessage()+"\n");
        return result;
    }
    
    
    public ArrayList<Text> getLogsList() {
        return logsList;
    }
}
