/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ecorp.easy.installer.logger;

import ch.qos.logback.core.PropertyDefinerBase;
import ecorp.easy.installer.AppConstants;
import java.util.UUID;

/**
 *
 * @author vincent
 */
public class LogPathPropertyDefiner extends PropertyDefinerBase{
    private static final String logFilePath = AppConstants.getWritableFolder()+UUID.randomUUID()+".log";
    
    
    /**
     * This is access for logback.xml
     * @return 
     */
    @Override
    public String getPropertyValue() {
        return logFilePath;
    }
    
    /**
     * This is access for FlashSceneController.java
     * @return 
     */
    public static String getLogFilePath(){
        return logFilePath;
    }
}
