/*
 * Copyright 2019-2020 - ECORP SAS 

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ecorp.easy.installer.models;

import ecorp.easy.installer.AppConstants;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Vincent Bourgmayer
 */
public class Command {
    String command; //String containing command to perform
    volatile boolean cancelled = false; //boolean to tell if it is cancelled or not
    Map<String, String> parameters; //Parameters to add to the command
    private int exitValue; //the exit value of the script. 
    private String shellOutput; //The shell text output of the execution of the script
    Process pc; //Process which run the command
    Step step; //Object which contain data about the command like next step, ...
    private final static Logger logger = LoggerFactory.getLogger(Command.class);
        
    //private final static Pattern SPACE_PATTERN = Pattern.compile("\\s+");
    /**
     * Constructor for instance in context of FlashThread
     * @param runScriptCmd
     * @param step 
     */
    public Command(String runScriptCmd, Step step){
        this.step = step;
        this.command = runScriptCmd+step.getScript();
        this.parameters = step.getParameters();
    }

    
    /**
     * Constructor for command object in context of PreparationThread
     * @param command 
    */
    public Command(String command){
        this.command = command;
        this.parameters = new HashMap<>();
    }
    
    /**
     *  Update a parameter only if already exist
     * @param key the key of parameter to update
     * @param value the new value of the param
     * @return false if param isn't already in the map
     */
    public boolean updateParameter(String key, String value){
        if(parameters.containsKey(key)){
            this.parameters.put(key, value);
            return true;
        }
        return false;
    }
    
    public String getCommand() {
        return command;
    }
    
    public void setCommand(String cmd){
        this.command = cmd;
    }
    public Map<String,String> getParameters() {
        return parameters;
    }

    public void setParameters(HashMap<String, String> parameters) {
        this.parameters = parameters;
    }
    
    public void addParameter(String key, String value){
        this.parameters.put(key, value);
    }
    
    public int getExitValue() {
        return exitValue;
    }

    public void setExitValue(int exitValue) {
        this.exitValue = exitValue;
    }

    public String getShellOutput() {
        return shellOutput;
    }

    public void setShellOutput(String shellOutput) {
        this.shellOutput = shellOutput;
    }
    
    /**
     * Build the string that contain full command with its parameters
     * @return String command and its parameters
     */
    public String[] getFinalCmd(){
        StringBuilder sb = new StringBuilder(command);
        if(parameters != null){
            parameters.values().forEach((param) -> {
                sb.append(" ").append(param);
            });
        }
        
        logger.debug("getFinalCmd(), Splitted command =  {}", sb.toString());
        return sb.toString().split(" ");
    }
    
    /**
     * @TODO: change name
     * @throws IOException
     * @throws InterruptedException 
     */
    public void execAndReadOutput()  throws IOException, InterruptedException{
        ProcessBuilder pb;
        if(AppConstants.isWindowsOs())
        {
            //pb = new ProcessBuilder();
            String[] commandArray= new String[parameters.size()+3];
            commandArray[0] = "cmd.exe";
            commandArray[1] = "/c";
            commandArray[2] = "\"\""+command+"\"";
           
            int i = 2;
            for(String param : parameters.values()){
                String quotedParam = "\""+param+"\"";
                if(i+1 == parameters.size()-1){
                    quotedParam += "\"";
                }
                commandArray[++i] = quotedParam;
            }
            pb = new ProcessBuilder(commandArray);
            logger.debug("command: {}", pb.command());
            //pb.directory(new File(AppConstants.getRootPath().substring(0, AppConstants.getRootPath().length()-2)));
            //logger.debug("Pb dir: {}", pb.directory());
            //pb.command(commandArray);
        }else{
             pb = new ProcessBuilder(getFinalCmd());
        }
        pb.redirectErrorStream(true);
        pc= pb.start();
        logger.info("Command's Process started");
        InputStream stdout = pc.getInputStream();
        InputStreamReader isr = new InputStreamReader (stdout);
        BufferedReader br = new BufferedReader(isr);
        
        StringBuilder sb = new StringBuilder();
        String line;
        try{
            while(pc.isAlive() && !cancelled){
                line = br.readLine();
                if( line != null && !line.equals("null") ){
                    sb.append("\n\n").append(line);
                    logger.debug("\n  (debug)"+line);
                }
            }
            this.exitValue = pc.exitValue();
        }catch(IOException e){
            logger.error("execAndReadOutput(), error = {}", e.toString() );
            this.exitValue = -1;
        }
        br.close();
        isr.close();

        this.shellOutput = sb.toString();
        if(pc.isAlive())       
            pc.destroy();
        
    }
    
    /**
     * This method try to solve issue of batch's args with space (windows only)
     * if arg contain os's path separator & Space
     * Try to split the given arg on Os's path separator
     * Then seek each part with space
     * when found, replace part with part surrounded with quote. 
     * 
     * NB: I think it would be easier with a regexp replace but I'm not good with regexp
     * @return the quoted arg string
     */
   /* private String quoteSpacedArg(String arg){
        if(arg.contains("\\") && SPACE_PATTERN.matcher(arg).find() ){
            String[] splittedArg = arg.split("\\\\"); //doesn't work with "AppConstants.Separator"
            for(String part : splittedArg){
                if(SPACE_PATTERN.matcher(part).find()){
                    arg = arg.replaceAll(part, "\""+part+"\"");
                }
            }
        }
        return arg;
    }*/
    
    
    public void cancel(){
        logger.info("cancel()");
        this.cancelled = true;
        if(pc != null && pc.isAlive())       
            pc.destroy();
    }
      

    /**
     * Return the new values for the UI but can be null if there is nothing to change in UI
     * @return null or StepUI instance
     */
    public StepUi getNewUIValues(){
        return step.getUI();
    }
    
    /**
     * Retourne the key define for output of script
     * the value is stored in this class property: shellOutput.
     * @return String can be null
     */

    public String getOutputKey(){
        return this.step.getOutput();
    }

    /**
     * Return the message associated with the exit Value
     * @return String can be null if exitValue is not define for this step
     */

    public String getErrorMsg(){
        return this.step.getKoCode().getOrDefault(this.exitValue, null);
    }

    public boolean isSuccess(){
        if(step.getOkCode() == null ) return (exitValue == 0);

        return this.step.getOkCode().keySet().contains(this.exitValue);
    }
    
    public String getNextCommandKey(){
        if(isSuccess() && !cancelled){
            return step.getAfterSuccess();
        }
        return step.getAfterFail();
    }
}
