/*
 * Copyright 2019-2020 - ECORP SAS 

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ecorp.easy.installer.models;

import java.util.AbstractMap.SimpleImmutableEntry;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import java.util.Set;

/**
 *
 * @author Vincent Bourgmayer
 */
public class DataBundle {
    private final static String STRING = "String";
    private final static String INT = "int";
    private final static String BOOLEAN = "boolean";
    private final static String LIST ="List<%s>";
    
    private final Map<String, SimpleImmutableEntry<String, Object>> datas;
    
    public DataBundle(){
        datas = new HashMap<>();
    }
    
    
    public void putBoolean(String key, Boolean value){
        datas.put(key, new SimpleImmutableEntry("boolean", value));
    }
    
    public void putInteger(String key, Integer value){
        datas.put(key, new SimpleImmutableEntry("int", value));
    }
    
    
    public void putString(String key, String value){
        datas.put(key, new SimpleImmutableEntry("String", value));
    }
    
    public void putObject(String key, Object value){
        datas.put(key, new SimpleImmutableEntry(value.getClass().getSimpleName(), value));
    }
    
    public void putList(String key, String itemClassName, List list){
        datas.put(key, new SimpleImmutableEntry(String.format(LIST, itemClassName), list));
    }
    
    /**
     * Return an List of Object
     * @param key the key of the list in the bundle
     * @param itemClassName the Object's type
     * @return 
     */
    public List getList(String key, String itemClassName){
        SimpleImmutableEntry p = datas.get(key);
        if(p != null && p.getKey().equals(String.format(LIST, itemClassName) )){
            return (List) p.getValue();
        }else{
            return null;
        }
    }
    
    public Object getObject(String key, String className){
        SimpleImmutableEntry p = datas.get(key);
        if(p != null && p.getKey() == className){
            return p.getValue();
        }else{
            return null;
        }
    }
    
    public Integer getInt(String key) {
        SimpleImmutableEntry p = datas.get(key);
        if(p != null && p.getKey() == INT){
            return (Integer) p.getValue();
        }else{
            return null;
        }
    }
    
    public String getString(String key){
        SimpleImmutableEntry p = datas.get(key);
        if(p != null && p.getKey() == STRING){
            return (String) p.getValue();
        }else{
            return null;
        }
    }
    
    public Boolean getBoolean(String key){
        SimpleImmutableEntry p = datas.get(key);
        if(p != null && p.getKey() == BOOLEAN){
            return (Boolean) p.getValue();
        }else{
            return null;
        }
    }
    
    public Set<String> getKeys(){
        return datas.keySet();
    }
    
}
