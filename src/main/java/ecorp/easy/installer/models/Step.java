/*
 * Copyright 2019-2020 - ECORP SAS 

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ecorp.easy.installer.models;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author Vincent Bourgmayer
 */
public class Step {
    private StepUi ui;
    private String script;  //script's file script
    private String output;  //Key of output to store as common parameter
    private String afterSuccess; // index of next script if succeed
    private String afterFail; //index of next script if failure
    private Map<Integer, String> okCode;
    private Map<Integer, String> koCode;
    private Map<String, String> parameters; //parameters to add to the script //@TODO think if is possible to set only one map in Thread instance instead of in each Step.
    
    public Step(){}
    
    public Step (Step s){
        this.script = s.script;
        afterSuccess = s.afterSuccess;
        afterFail = s.afterFail;
        ui = s.ui;
        okCode = s.okCode;
        koCode = s.koCode;
        output = s.output;
        if(s.parameters != null){
            parameters = new LinkedHashMap<>();
            for(Map.Entry<String, String> param : s.parameters.entrySet()){
                parameters.put(param.getKey(), param.getValue());
            }
        }
    }

    public String getScript() {
        return script;
    }

    public void setScript(String name) {
        this.script = name;
    }

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }

    public Map<String, String> getParameters() {
        return parameters;
    }

    public void setParameters(LinkedHashMap<String, String> parameters) {
        this.parameters = parameters;
    }
     public String getAfterSuccess() {
        return afterSuccess;
    }

    public void setAfterSuccess(String afterSuccess) {
        this.afterSuccess = afterSuccess;
    }

    public String getAfterFail() {
        return afterFail;
    }

    public void setAfterFail(String afterFail) {
        this.afterFail = afterFail;
    }

    public Map<Integer, String> getOkCode() {
        return okCode;
    }

    public void setOkCode(Map<Integer, String> okCode) {
        this.okCode = okCode;
    }

    public Map<Integer, String> getKoCode() {
        return koCode;
    }

    public void setKoCode(Map<Integer, String> koCode) {
        this.koCode = koCode;
    }
    
    /**
     * Get the StepUI object which encapsulate new value for UI.
     * It will be "null" if UI must not change.
     * @return null or StepUI instance
     */
    public StepUi getUI() {
        return ui;
    }

    public void setUI(StepUi ui) {
        this.ui = ui;
    }
}
