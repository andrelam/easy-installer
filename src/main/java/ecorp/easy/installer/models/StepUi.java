/*
 * Copyright 2019-2020 - ECORP SAS 

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ecorp.easy.installer.models;

import java.util.List;

/**
 *
 * @author vincent Bourgmayer
 */
public class StepUi{
    private String type; //Can be: error, load or action
    private List<String> instructions; //Instruction of the step
    private String title; // Title of the step
    private String stepNumber; //It correspond more to the state of progression. It should be like: 1/7 not just: 1
    final private String titleIconName;
    final private int averageTime; //in second. The average time required to finsh the step. It only concern 'load' type.
    /**
     * Constructor for StepUI object
     * @param type type of step ("action" if user action is needed or "load" if no user action is needed)
     * @param instructions instructions of the step
     * @param title title of the current step
     * @param titleIconName icon to show beside to the title. Only for action type. Either provide null.
     * @param stepNumber the number of the step in the whole process
     * @param averageTime The number of second needed to finish the step (it's an average)
     */
    public StepUi(String type, List<String> instructions, String title, String titleIconName, String stepNumber, int averageTime){
        this.type = type;
        this.instructions = instructions;
        this.title = title;
        this.stepNumber = stepNumber;
        this.titleIconName = titleIconName;
        this.averageTime = averageTime;
    }
    
    /**
     * get the file name of the icon beside the title
     * @return return null if no value provided at constructor which is the case if type is "load" instead of "action"
     */
    public String getTitleIconName(){
        return this.titleIconName;
    }
   
    
    /**
     * Get the average time required to finish the task
     * @return int the value or -1 if no value
     */
    public int getAverageTime(){
        return this.averageTime;
    }
    
        /**
     * get the type of Step from UI point of view.
     * It has mainly impact on background color of the flash box
     * @return can be null but should be "error", "load" or "action"
     */
    public String getType() {
        return type;
    }

    /**
     * Define the type of Step from UI point of view.
     * It has mainly impact on background color of the flash box
     * @param type should be "error", "load" or "action"
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * Get the instruction to print for user
     * @return String, but can be null.
     */
    public List<String> getInstructions() {
        return instructions;
    }

    
    public void setInstruction(List<String> instructions) {
        this.instructions = instructions;
    }

    
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Return the step number as progress.
     * It will be like "1/7" or something like that.
     * @return String
     */
    public String getStepNumber() {
        return stepNumber;
    }

    public void setStepNumber(String stepnumber) {
        this.stepNumber = stepnumber;
    }
}
