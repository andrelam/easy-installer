/*
 * Copyright 2019-2020 - ECORP SAS 

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ecorp.easy.installer.tasks;

import ecorp.easy.installer.AppConstants;
import ecorp.easy.installer.exceptions.TooManyDevicesException;
import ecorp.easy.installer.models.Command;
import ecorp.easy.installer.models.Device;
import java.util.regex.Pattern;

import javafx.concurrent.Task;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class provide task which run ADB command in background to detect that a device is reachable
 * @author vincent Bourgmayer
 * @author Ingo
 */
public class DeviceDetectionTask extends Task<Device>{
    final String CMD_adbDevices = "devices";    
    private final static Logger logger = LoggerFactory.getLogger(DeviceDetectionTask.class);
    private final static Pattern LINEBREAK_PATTERN = Pattern.compile("\\R");
    private final static Pattern SPACE_PATTERN = Pattern.compile("\\s+");

    @Override
    protected Device call() throws Exception{
        Device detectedDevice = null;

        detectedDevice = runAdbDevicesCmd(AppConstants.getADBFolderPath()+"adb");

        if(detectedDevice != null){
            logger.debug("call(), result: Device found");
        }
        return detectedDevice;
    }
    
    /**
     * Run "adb devices -l" command
     * @param baseCmdString
     * @return
     * @throws Exception 
     */
    private Device runAdbDevicesCmd(String baseCmdString) throws Exception{
        logger.info("runADBDevicesCmd({})", baseCmdString);
        
        Command cmd = new Command(baseCmdString);
        cmd.addParameter("1", CMD_adbDevices);
        cmd.addParameter("2", "-l");
        cmd.execAndReadOutput();     
        

        Device detectedDevice = null;
        String[] outputs = LINEBREAK_PATTERN.split(cmd.getShellOutput());
        logger.debug(" raw shell outputs = {} ", cmd.getShellOutput());
        if(outputs.length <=1){
            logger.info("  Waiting");
            Thread.sleep(2250);
            return null;
        }
        int counter =0;
        for(String s : outputs){
            Device deviceFound = checkAdbDevicesResult(s);
            if(deviceFound != null){
                detectedDevice = deviceFound;
                ++counter;
            }
        }
        if(counter > 1){
            logger.info("  Too many devices detected");
            throw new TooManyDevicesException(counter);
        }
        if(detectedDevice==null) {
            logger.info("  waiting");
            Thread.sleep(2250);
        }
        return detectedDevice;
    }
    
    
    
    /**
     * Analyse one line from result of "ADB devices -l" command
     * @param resultLine
     * @return the adb's device's id if found else it return empty string
     */
    private Device checkAdbDevicesResult(String resultLine){
        logger.debug("checkAdbDevicesResult({})", resultLine);
        boolean deviceFound = false;
        Device result = null;
        
        //Split string on each space
        String[] datas = SPACE_PATTERN.split(resultLine);
        
        //loop over each subString
        for(String stringPart : datas){
            logger.debug("  Current subString : "+stringPart);
            //Detect the line that says that device is found
            if(!stringPart.isEmpty() && (stringPart.endsWith("device") ||stringPart.endsWith("recovery") ) ){
                logger.info("  Device has been found");
                deviceFound = true;
                result = new Device(datas[0]); //get adb id
            }
            //Read data and store them in Device object
            if(deviceFound){
               if(stringPart.contains("product:")){
                    logger.debug("  \"product\" keyword has been found");
                    result.setName(stringPart.substring("product:".length() ));
               }else if(stringPart.contains("model:")){
                     logger.debug("  \"model\" keyword has been found");
                    result.setModel(stringPart.substring("model:".length() ));
               }else if(stringPart.contains("device:")){
                    logger.debug("  \"device\" keyword has been found");
                    result.setDevice(stringPart.substring("device:".length() ));
               }
            }
        }
        return result;
    }
}
