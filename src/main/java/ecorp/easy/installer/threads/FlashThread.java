/*
 * Copyright 2019-2020 - ECORP SAS 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ecorp.easy.installer.threads;

import ecorp.easy.installer.AppConstants;
import ecorp.easy.installer.models.Device;
import ecorp.easy.installer.models.Command;
import ecorp.easy.installer.models.DataBundle;
import ecorp.easy.installer.models.StepUi;
import ecorp.easy.installer.utils.IFlashHandler;
import javafx.application.Platform;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 *
 * @author Vincent Bourgmayer
 * @author Ingo
 */
public class FlashThread extends Thread {
    final protected static char COMMON_PARAM_IDENTIFIER = '$';//allow to identify when an param of a step is to load from common parameter
    final protected static Pattern REGEX_FIND_PARAM = Pattern.compile("\\$\\{(.*?)\\}");
    private final static Logger logger = LoggerFactory.getLogger(FlashThread.class);
    //private final FlashLogger logger;
    boolean running = false;
    boolean cancelled = false;
    boolean sendIssueLog = false; //if true then the app must write the file for issue. @TODO replace the word "send" by a word more accurate.
    boolean sendRecordLog = true;
    boolean success = false; //used to run script one after one only if success @TODO check is still usefull
    boolean atLeastOneError = false; //Say if there has been at least one issue during all the process
    final IFlashHandler application;
    final Object pauseLock;

    final Device device;
    protected final Map<String, String> commonParameters; // this is parameters that are define by a script output and that can be use as parameter by another script
    protected final Map<String, Command> commands;
    protected String currentStepCode = "";
    protected final String firstCommandKey;

     /**
     * Constructor for flashThread
     * @param controller This is the controller which change the UI depending on the flash thread result
     * @param firstCommandKey The command key to use to get the first step to perform
     * @param pauseLock a semaphor to stop allow to pause and restart the thread
     * @param device Object containing device info
     */
    public FlashThread(IFlashHandler controller, String firstCommandKey, Object pauseLock, Device device){
        this.commands = new HashMap<>();
        this.commonParameters = new HashMap<>();
        this.firstCommandKey = firstCommandKey; //this is the key of the command's hashmap
        this.application = controller;
        this.pauseLock = pauseLock;
        this.device = device;
        String sourcePath = AppConstants.getSourcesFolderPath();

        //setParameters
        this.commonParameters.put("SOURCES_PATH", sourcePath);
        this.commonParameters.put("TWRP_IMAGE_PATH", sourcePath+AppConstants.getTwrpImgPath());
        this.commonParameters.put("ARCHIVE_PATH", sourcePath+AppConstants.getEArchivePath());
        this.commonParameters.put("ADB_FOLDER_PATH", AppConstants.getADBFolderPath());
        this.commonParameters.put("HEIMDALL_FOLDER_PATH", AppConstants.getHeimdallFolderPath());
        this.commonParameters.put("DEVICE_ID", device.getAdbId());
        this.commonParameters.put("DEVICE_DEVICE", device.getDevice());
        this.commonParameters.put("JAVA_FOLDER_PATH", AppConstants.JavaHome);
    }

    
    /**
     * Add a command associated to a specific key
     * @param key must unique or the value associated with will be replaced by new one
     * @param command Command to add
     */
    public void addCommand(String key, Command command){
        if(command != null && key!= null)
            commands.put(key, command);
    }
    
    /**
     * Handle result of a command
     * @param command
     * @return boolean true is success false either
     */
    private boolean handleResult(final Command command){
        logger.info("handleResult()");
        //read result from command object
        final int exitValue = command.getExitValue();
        final String shellOutput = command.getShellOutput();
        
        logger.debug("  Shell output= {}\nexit value = {}\n", shellOutput, exitValue);


        if(command.isSuccess()){
            String outputKey = command.getOutputKey();
            //If an output is expected from succeed script
            if( outputKey != null && outputKey.charAt(0) == COMMON_PARAM_IDENTIFIER){
                final int lastIndex = shellOutput.lastIndexOf("\n");
                final int length = shellOutput.length();
                final String shellOutputCleaned = shellOutput.substring(lastIndex+1, length);
                logger.debug("shell output cleaned : "+shellOutputCleaned);
                this.commonParameters.put(outputKey.replace("${","").replace("}", ""), shellOutputCleaned);
            }
            
            return true;
        }else{ //fails case
            String errorMsgKey = command.getErrorMsg();
            if(errorMsgKey == null || errorMsgKey.isEmpty()){
                errorMsgKey = "script_error_unknown";
            }
            logger.warn("Exit value means: "+errorMsgKey );
            showError(errorMsgKey);
            return false;
        }
    }

    /**
     * send info to app to make it in error mode
     * @param errorMsgKey the error message translation key to send to UI
     */
    protected void showError(String errorMsgKey) {
        
        //@TODO remove below specific code
        final DataBundle bundle = new DataBundle();
        bundle.putString("errorMsgKey", errorMsgKey); //@Todo: put errorMsgKey as staticaly defined somewhere
        Platform.runLater(() -> {
            application.onFlashError(bundle);
        });
    }
    
    /**
     * stop the thread
     * @param sendLog if true, Logger will send data in logger
     */
    synchronized public void cancel(boolean sendLog){
        logger.info("cancel()");
        if(running && commands.containsKey(currentStepCode)){ //@TODO the second part of the test won't work, as we include some "error" steps. So the commands.size isn't good.
            Command cmd = commands.get(currentStepCode);
            if(cmd != null) cmd.cancel();
        }
        else{ //if in success
            showError("flash_process_cancelled");
        }
        cancelled = true;
        if(!sendLog){
            sendIssueLog = false;
            sendRecordLog = false;
        }
    }

    
    /**
     * Finish the thread: send data to logger if needed, set running to false and try to start a new thread.
     */
    synchronized public void finish(){
        //if(sendIssueLog ) ((FlashLogger) logger).writeToFile("Flash");
        running = false;
        
        final DataBundle bundle = new DataBundle();
        bundle.putBoolean("atLeastOneError", atLeastOneError);
        bundle.putBoolean("sendRecordLog", sendRecordLog);
            
        Platform.runLater(() ->{
            application.onFlashThreadEnd(bundle);
        });
    }
    
    /**
     * @return true if thread is running else it return false 
     */
    synchronized public boolean isRunning(){
        return this.running;
    }
    

    /**
     * Execute some code before to execute the current command
     * It is runned in the While loop which loops over commands
     * It's executed in a Try catch.
     * @throws java.lang.Exception
     */
    protected void doBeforeToRunCommand() throws Exception{
        //Update UI
        final Command cmd = commands.get(currentStepCode);
        final DataBundle bundle = new DataBundle();
        
        //Prepare datas for UI
        final String stepType = cmd.getNewUIValues().getType();
        bundle.putString("stepType", stepType);

        if( stepType.equals(AppConstants.USER_ACTION_KEY) || stepType.equals(AppConstants.LOAD_KEY)){
            //UpdateUI
            final StepUi newUIValues = (StepUi) cmd.getNewUIValues();
            if(newUIValues != null) { 
                //@todo: Must define bundle key as a static String at a single place.
                if(stepType.equals(AppConstants.USER_ACTION_KEY)){
                    bundle.putString("titleIconName", newUIValues.getTitleIconName());
                }else{
                    bundle.putInteger("averageTime", newUIValues.getAverageTime());
                }
                bundle.putString("stepTitle", newUIValues.getTitle());
                bundle.putList("stepInstructions", String.class.getSimpleName(), newUIValues.getInstructions());

            }
        }
        
        //Update UI
        Platform.runLater(()->{
            logger.debug("runLater-> Update UI for"+cmd.getCommand());
            application.onStepStart(bundle);
        });
        
        if(stepType.equals(AppConstants.ASK_ACCOUNT_KEY)  || stepType.equals(AppConstants.UNLOCK_OEM_KEY))
        {
            try{
                synchronized(pauseLock){ pauseLock.wait(); }
            }catch (InterruptedException e) {
                logger.error("error = "+e.toString());
            }
        }
    }
    
    /**
     * Execute some code after the current command has been 
     * It is runned in the While loop which loops over commands
     * It's executed in a Try catch.
     * @throws java.lang.Exception
     */
    protected void doAfterToRunCommand() throws Exception{
        atLeastOneError =  (atLeastOneError || !success || cancelled);
        sendIssueLog = (atLeastOneError && !cancelled);
    }
    

    /**
     * Do some code when an error has been raised
     * It isn't executed in a try catch;
     * @param e The exception raised for the error
     */
    protected void onErrorRaised(Exception e) {
        showError("java_error_unknow");
        //send error in log rather that in UI
        sendIssueLog = true;
        atLeastOneError = true;
        logger.error("Java exception: "+ e.toString());
    }

    /**
     * Do some code at the real end of "run" methodremove-fs-
     * It is not executed in a Try Catch;
     */
    protected void onRunEnd() {
        final boolean issueEncountered = atLeastOneError;
        final DataBundle bundle = new DataBundle();
        bundle.putBoolean("isFlashed", (success && !cancelled && !issueEncountered));
        Platform.runLater(() ->{
            //Do not use raw success. Because if error declared after a step has succeed then it will be considered as a succes
             application.onFlashStop( bundle);
        });
    }

    protected void runInitialization() {
        running = true;
    }
    
    @Override
    public void run(){
        if(commands.isEmpty()) return;
        runInitialization();
        try{
            //execute scripts
            String nextCommandKey = firstCommandKey;
            while(nextCommandKey != null ){
                currentStepCode = nextCommandKey;
                final Command cmd = commands.get(nextCommandKey);
                
                updateParameters();
                //UpdateUI
                doBeforeToRunCommand();
                final String stepType = cmd.getNewUIValues().getType();
                if( stepType.equals(AppConstants.USER_ACTION_KEY) 
                    || stepType.equals(AppConstants.LOAD_KEY) )
                {
                    logger.debug("Run(), Command = "+cmd.getCommand());
                    cmd.execAndReadOutput();
                    success = handleResult(cmd);
                }else{
                    success = true;
                }
    
                doAfterToRunCommand();
                
                nextCommandKey = cmd.getNextCommandKey();
            }

        }catch(Exception e){
            onErrorRaised(e);
        }
        onRunEnd();
    }
    
    /**
     * Return the number of step of the full process
     * @return 
     */
    public int getCommandsSize(){
        return this.commands.size();
    }
    
    
    /**
     * Update parameters of the current command
     * It is called before to execute the command
     */
    protected void updateParameters(){
        final Command cmd = commands.get(currentStepCode);
        //Update Parameters
        if(cmd.getParameters() != null){ //@TODO: remove functionnal and rewrite it as it was before with simple loop.
            cmd.getParameters().entrySet().stream().filter((param) -> (param.getValue().contains("$"))).forEachOrdered((param) -> {
                Matcher matcher = REGEX_FIND_PARAM.matcher(param.getValue());
                while(matcher.find()){
                    cmd.updateParameter(param.getKey(), param.getValue().replace(matcher.group(0), commonParameters.get(matcher.group(1))));
                }
            });

            logger.debug("updateParameters(), Parameters = "+cmd.getParameters().toString());
        }     
    }
}
