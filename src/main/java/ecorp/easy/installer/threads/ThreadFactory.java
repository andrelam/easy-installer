/*
 * Copyright 2019-2020 - ECORP SAS 

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ecorp.easy.installer.threads;
import ecorp.easy.installer.models.StepUi;
import ecorp.easy.installer.AppConstants;
import ecorp.easy.installer.models.Command;
import ecorp.easy.installer.models.Device;
import ecorp.easy.installer.models.ProcessMould;
import ecorp.easy.installer.models.Step;
import ecorp.easy.installer.utils.IFlashHandler;
import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidParameterException;
import java.util.LinkedHashMap; //used instead of HashMap to conserve order
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.HashMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.yaml.snakeyaml.Yaml;

/**
 * This is the class in charge of Thread's creation
 * @TODO: in BuildFlashThread Method, let the Steps load from yaml to be a kind of static ressource used as mould. 
 * And create parameters field in Command so the Parameters of Step object stay unchanged and aren't specific to each Thread.
 * We'll safe memory by doing that.
 * @author Vincent
 */
public class ThreadFactory {
    private final static Logger logger = LoggerFactory.getLogger(ThreadFactory.class);
    private ProcessMould flashMould;
    private final HashMap<String, String> sourcesToDownload;
    private Device device;
    private final String yamlFolderPath;
    
    public ThreadFactory(String yamlFolderPath){
        this.yamlFolderPath = yamlFolderPath;
        this.sourcesToDownload = new HashMap<>();
    }
    
    /**
     * Prepare the Factory to build thread for another model
     * @param device Device object that encapsulate data about the device to flash
     * @return false if it fails somewhere (i.e: no YAML found with this modelName. Else it return true.
     * It's maybe not be not very precise but for now it'll be efficient enough.
     */
    public boolean changeMould(Device device){
        if(device == null) return false;
        
        this.device = device;
   
        return loadYAMLFile();
    }
    
    /**
     * Load the YAMLFile for the device's model to flash
     * @return false if there is an issue or if no flashMould is built
     */
    private boolean loadYAMLFile(){
        logger.info("loadYAMLFile()");
        final String modelName = device.getDevice(); //Do not use device.getModel as it doesn't return the expected value
        if(modelName == null || modelName.isEmpty()){
            return false;
        }
        
        try{
            Yaml yaml = new Yaml ();
            
            //load config file
            InputStream is = getClass().getResourceAsStream(yamlFolderPath+modelName+".yml");
            Map yamLContent=  (Map)yaml.load(is);
            
            if(yamLContent == null) throw new NullPointerException("flash content load from yaml is empty");
            flashMould = new ProcessMould(modelName);
            
            loadFlashProcess((Map) yamLContent.get("flash"));
            is.close();
            

            //Load config file specific to Flash station
            //DO NOT USE classLoader() to load resource!
            is = getClass().getResourceAsStream(yamlFolderPath+modelName+"_fs.yml");
            yamLContent=  (Map)yaml.load(is);
            if(yamLContent == null) throw new NullPointerException("extra content load from yaml is empty");
            
            loadFlashExtra((Map) yamLContent.get("flash"));
                
            loadSourcesToDownload((Map) yamLContent.get("sources")); 
                
            is.close();
            
        }catch(IOException | NullPointerException e){
            logger.error("modelName = {}, error= {}", modelName, e.toString());

            return false;
        }
        return true;
    }
    
    /**
     * Load FlashProcess element from yaml file loaded
     * @param steps Map containing data from YAML
     */
    protected void loadFlashProcess( Map steps){
        logger.info("loadFlashProcess( ... ) ");
        if(steps == null || steps.isEmpty()){
            throw new InvalidParameterException("Flash steps loaded from YAML are invalid");
        }
        
        Set<String> keys = steps.keySet();
 
        String scriptExtension = AppConstants.OsName.toLowerCase().contains("windows") ? ".bat" : ".sh";           
        for(String key: keys){
            logger.debug("Key = "+key);
            Map stepData = (Map) steps.get(key);
            Step step = new Step();
            
            step.setScript((String) stepData.get("script")+scriptExtension);

            Map codes = (Map) stepData.get("codes");
            if(codes != null){
                step.setOkCode ( (Map) codes.get("ok") );
                step.setKoCode ( (Map) codes.get("ko") );
            }
            step.setParameters ((LinkedHashMap<String, String>) stepData.get("parameters") );

            step.setOutput ((String) stepData.get("output") );
            step.setAfterSuccess ((String) stepData.get("succeed") );
            step.setAfterFail ((String) stepData.get("failed") );
            this.flashMould.addStep(key, step);
        }
    }
    
    /**
     * Load extra element for Flash process (it contains mainly UI's element)
     * @TODO: make entries's key a final static to be store only at one place
     * @param steps  Map Loaded from YAML
     */
    protected void loadFlashExtra( Map steps){
        logger.info("loadFlashExtra(...)");
        if(steps == null || steps.isEmpty()){
            throw new InvalidParameterException("Flash steps's extra loaded from YAML are invalid");
        }

        for(String key : (Set<String>) steps.keySet() ){
            Map subObj = (Map) steps.get(key);
            if(subObj == null) continue;

            StepUi stepUI = null;

            Map uiProperties = (Map) subObj.get("ui");
            if(uiProperties != null){
                String type = (String) uiProperties.get("type");
                List<String> instructions = (List<String>) uiProperties.get("instruction");
                String title = (String) uiProperties.get("title");
                String titleIconName= (String)uiProperties.get("titleIcon");
                String stepNumber = (String)uiProperties.get("stepNumber");
                Integer averageTime = (Integer) uiProperties.get("averageTime");          
                stepUI = new StepUi(type, instructions, title, titleIconName, stepNumber, averageTime != null ? averageTime:-1);
            }

            Step step = flashMould.getSteps().get(key);
            if(step != null){
                step.setUI(stepUI );
            }
        }
    }
    
    /**
     * Load Preparation steps from yaml
     * @param sources Map containing steps of preparation
     */
    protected void loadSourcesToDownload( Map sources){
        logger.info("loadSourcesToDownload(...)");
        
        if(sources == null || sources.isEmpty()){ throw new InvalidParameterException("Preparation steps loaded from YAML are invalid"); }
        
        for(String key : (Set<String>) sources.keySet() ){
            Map source = (Map) sources.get(key); 
            if(key.equals("rom")){
                AppConstants.setEArchivePath((String) source.get("filePath"));
            }
            if(key.equals("twrp")){
                AppConstants.setTwrpImgPath((String) source.get("filePath"));
            }
            sourcesToDownload.put((String) source.get("url"), (String) source.get("filePath"));
        }
    }
    
    public HashMap<String, String> getSourcesToDownload(){
        return sourcesToDownload;
    }

    
    /**
     * Build a FlashThread
     * @param application The element that handle exchange with the thread
     * @param pauseLock The lock used to pause/unpause the thread
     * @return FlashThread
     */
    public FlashThread buildFlashThread( IFlashHandler application, Object pauseLock){
        
        if(flashMould == null || flashMould.getSteps() == null || device == null) return null;
        
        FlashThread result = new FlashThread(application, "f1", pauseLock, device);
        
        
        flashMould.getSteps().entrySet().forEach((entry) -> {
            result.addCommand(entry.getKey(), new Command(AppConstants.getScriptsFolderPath(),  new Step( entry.getValue() ) ) );
        });
        return result;
    }
    
    /**
     * get  the number of steps inside the current ProcessMould
     * @return 0 if processMould is null
     */
    public int getStepsCount(){
        if(flashMould == null) return 0;
        return flashMould.getSteps().size();
    }
}
